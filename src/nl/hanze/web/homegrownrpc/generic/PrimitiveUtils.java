package nl.hanze.web.homegrownrpc.generic;

import java.util.HashMap;
import java.util.Map;

/**
 * Utility class for primitive types.
 * 
 * @author "Johan Mulder"
 */
public class PrimitiveUtils
{
	// Contains the mapping between primitive type names and the corresponding
	// autoboxing class names.
	private static Map<String, String> primitiveMap;

	// Static initialization block.
	static
	{
		primitiveMap = new HashMap<>();
		primitiveMap.put("boolean", "java.lang.Boolean");
		primitiveMap.put("int", "java.lang.Integer");
		primitiveMap.put("float", "java.lang.Float");
		primitiveMap.put("byte", "java.lang.Byte");
		primitiveMap.put("char", "java.lang.Character");
		primitiveMap.put("long", "java.lang.Long");
		primitiveMap.put("short", "java.lang.Short");
		primitiveMap.put("double", "java.lang.Double");
	}

	/**
	 * Map a primitive name to the autoboxing class name.
	 * 
	 * @param primitiveTypeName
	 * @return
	 */
	public static String mapPrimitiveToClass(String primitiveTypeName)
	{
		return primitiveMap.get(primitiveTypeName);
	}

	/**
	 * Determine if a given type name is a primitive type name.
	 * 
	 * @param primitiveTypeName
	 * @return
	 */
	public static boolean isPrimitive(String primitiveTypeName)
	{
		return primitiveMap.containsKey(primitiveTypeName);
	}
}
