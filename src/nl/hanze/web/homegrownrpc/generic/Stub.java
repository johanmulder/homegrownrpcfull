package nl.hanze.web.homegrownrpc.generic;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;

@SuppressWarnings("rawtypes")
public class Stub
{
	private String strIP;
	private int port;

	public void setSkelLocation(String strIP, int port) throws Exception
	{
		this.strIP = strIP;
		this.port = port;
	}

	protected Object invokeSkel(String methodName, Class[] parameterTypes, Object[] parameterValues)
			throws Exception
	{
		if (parameterTypes != null && parameterValues != null)
			checkNumParameters(parameterTypes, parameterValues);
		checkParameterTypesAndValues(parameterTypes, parameterValues);

		try (Socket socStub = new Socket(strIP, port))
		{
			ObjectOutputStream oos = new ObjectOutputStream(socStub.getOutputStream());
			oos.writeObject(methodName);
			oos.writeObject(parameterTypes);
			oos.writeObject(parameterValues);
			oos.flush();

			ObjectInputStream ois = new ObjectInputStream(socStub.getInputStream());
			// If ois.readBoolean() returns true, the result type is void.
			return ois.readBoolean() ? null : readObject(ois);
		}
	}

	/**
	 * Check if the number of parameters is equal to the number of parameter
	 * values.
	 * 
	 * @param parameterTypes
	 * @param parameterValues
	 * @throws Exception
	 */
	private void checkNumParameters(Class[] parameterTypes, Object[] parameterValues)
			throws Exception
	{
		if (parameterTypes.length != parameterValues.length)
			throw new Exception("Bad parameters: the same amount of types need to "
					+ "be given for the amount of parameters");
	}

	/**
	 * Check if both the parameter types and values are given.
	 * 
	 * @param parameterTypes
	 * @param parameterValues
	 * @throws Exception
	 */
	private void checkParameterTypesAndValues(Class[] parameterTypes, Object[] parameterValues)
			throws Exception
	{
		if (parameterTypes != null ^ parameterValues != null)
			throw new Exception("Both parameter types and parameter types need to be given.");
	}

	/**
	 * Read an object from the given object input stream.
	 * 
	 * @param ois
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	private Object readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		Class resultType = (Class) ois.readObject();
		return resultType.isPrimitive() ? readPrimitive(ois, resultType.getName()) : ois
				.readObject();
	}

	/**
	 * Read a primitive from the object input stream.
	 * 
	 * @param ois The ObjectInputStream to read from.
	 * @param typeName The name of the primitive type.
	 * @return The read data.
	 * @throws IOException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	private Object readPrimitive(ObjectInputStream ois, String typeName) throws IOException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		// The method name should be equal to read+type name
		String methodName = "read" + typeName;
		for (Method method : ois.getClass().getMethods())
			if (method.getName().equalsIgnoreCase(methodName))
				return method.invoke(ois);
		return null;
	}

}