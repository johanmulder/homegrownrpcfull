package nl.hanze.web.homegrownrpc.generic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Method;

public class StubGenerator
{
	private StringBuffer stbTemp;
	private String fullInterfaceName;
	private String nameStub;
	private String packageStub;
	private String locationStub;

	public StubGenerator(String fullInterfaceName, String baseLocationStub)
	{
		this.fullInterfaceName = fullInterfaceName;
		this.nameStub = getInterfaceName() + "Stub";
		this.packageStub = getPackageName(false);
		this.locationStub = baseLocationStub + File.separatorChar + getPackageName(true);
	}

	public void generateStub() throws Exception
	{
		stbTemp = new StringBuffer();
		generatePackageName();
		generateEmptyLine();
		generateClassLine();
		Method[] methods = searchMethods();
		for (Method method : methods)
		{
			generateEmptyLine();
			generateMethod(method);
		}
		generateEmptyLine();
		generateLastLine();
		saveGeneratedClass();
	}

	private void generatePackageName()
	{
		if (packageStub != null && packageStub.length() != 0)
		{
			stbTemp.append("package ");
			stbTemp.append(packageStub);
			stbTemp.append(";\n");
		}
	}

	private void generateClassLine()
	{
		stbTemp.append("public class ");
		stbTemp.append(nameStub);
		stbTemp.append(" extends nl.hanze.web.homegrownrpc.generic.Stub");
		stbTemp.append(" implements ");
		stbTemp.append(fullInterfaceName);
		stbTemp.append(" {");
		generateEmptyLine();
	}

	private Method[] searchMethods() throws Exception
	{
		return Class.forName(fullInterfaceName).getMethods();
	}

	/**
	 * Generate and add the complete method.
	 * 
	 * @param method
	 */
	private void generateMethod(Method method)
	{
		String returnType = method.getReturnType().getCanonicalName();
		boolean isPrimitive = PrimitiveUtils.isPrimitive(returnType);
		boolean isVoid = returnType.equals("void");
		String methodName = method.getName();
		@SuppressWarnings("rawtypes")
		Class[] params = method.getParameterTypes();
		boolean hasParameters = params.length != 0;

		stbTemp.append("\tpublic ");
		// Return type.
		addMethodReturnType(isVoid, returnType);

		// Method name
		stbTemp.append(methodName + "(");

		// Parameters.
		if (hasParameters)
			for (int i = 0; i < params.length; i++)
			{
				stbTemp.append(params[0].getCanonicalName());
				stbTemp.append(" param");
				stbTemp.append(i);
				if (i != params.length - 1)
					stbTemp.append(", ");
			}
		stbTemp.append(") throws Exception {\n\t\t");

		// Method body
		addMethodBody(returnType, methodName, hasParameters, params, isPrimitive, isVoid);
	}

	/**
	 * Add the method return type of a generated method.
	 * 
	 * @param isVoid If set to true, it'll add "void"
	 * @param returnType If isVoid is false, this type will be added.
	 */
	private void addMethodReturnType(boolean isVoid, String returnType)
	{
		if (isVoid)
			stbTemp.append("void ");
		else
			stbTemp.append(returnType + " ");

	}

	/**
	 * Add the method body to the generated class.
	 * 
	 * @param returnType
	 * @param methodName
	 * @param hasParameters
	 * @param params
	 * @param isPrimitive
	 * @param isVoid
	 */
	private void addMethodBody(String returnType, String methodName, boolean hasParameters,
			@SuppressWarnings("rawtypes") Class[] params, boolean isPrimitive, boolean isVoid)
	{
		if (!isVoid)
		{
			stbTemp.append("return (");
			stbTemp.append(isPrimitive ? PrimitiveUtils.mapPrimitiveToClass(returnType)
					: returnType);
			stbTemp.append(") ");
		}
		stbTemp.append("invokeSkel(");
		// Method name.
		stbTemp.append("\"");
		stbTemp.append(methodName);
		stbTemp.append("\", ");

		if (hasParameters)
			addMethodParameters(params);
		else
			stbTemp.append("null, null");
		stbTemp.append(");\n\t}\n");
	}

	/**
	 * Add the method parameters to the StringBuffer.
	 * 
	 * @param params
	 */
	private void addMethodParameters(@SuppressWarnings("rawtypes") Class[] params)
	{
		stbTemp.append("new Class[] { ");
		// Parameter classes.
		for (int i = 0; i < params.length; i++)
		{
			stbTemp.append(params[i].getCanonicalName());
			stbTemp.append(".class");
			if (i != params.length - 1)
				stbTemp.append(", ");
		}

		// Parameter objects.
		stbTemp.append(" }, new Object[] { ");
		for (int i = 0; i < params.length; i++)
		{
			stbTemp.append("param" + i);
			if (i != params.length - 1)
				stbTemp.append(", ");
		}

		stbTemp.append(" }");

	}

	private void generateEmptyLine()
	{
		stbTemp.append("\n");
	}

	private void generateLastLine()
	{
		stbTemp.append("}\n");
	}

	private void saveGeneratedClass() throws Exception
	{
		// Create the directory tree.
		File outputDir = new File(locationStub);
		outputDir.mkdirs();
		// Create the output file.
		File outputFile = new File(locationStub + "/" + nameStub + ".java");
		outputFile.createNewFile();
		// Write the stbTemp buffer to the file.
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
		writer.write(stbTemp.toString());
		writer.close();
	}

	private String getInterfaceName()
	{
		int i = fullInterfaceName.lastIndexOf('.');
		if (i == -1)
			return fullInterfaceName;
		i++;
		return fullInterfaceName.substring(i);
	}

	private String getPackageName(boolean useSlashes)
	{
		int i = fullInterfaceName.lastIndexOf('.');
		if (i == -1)
			return "";
		if (useSlashes)
			return fullInterfaceName.substring(0, i).replace('.', File.separatorChar);
		else
			return fullInterfaceName.substring(0, i);
	}

	public static void main(String[] args) throws Exception
	{
		StubGenerator sg = new StubGenerator("nl.hanze.web.homegrownrpc.addressbook.AddressBook",
				"/tmp");
		sg.generateStub();
	}

}
