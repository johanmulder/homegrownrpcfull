package nl.hanze.web.homegrownrpc.generic;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;

@SuppressWarnings("rawtypes")
public class Skel
{
	private int port;
	private Object obj;
	private ServerSocket ssoSkel;

	public void setPort(int port)
	{
		this.port = port;
	}

	public void setImplementation(Object obj)
	{
		this.obj = obj;
	}

	public void listen() throws Exception
	{
		ssoSkel = new ServerSocket(port);

		while (true)
		{
			invokeMethod(ssoSkel.accept());
		}
	}

	private void invokeMethod(Socket s) throws Exception
	{
		ObjectInputStream ois = new ObjectInputStream(s.getInputStream());
		String methodName = (String) ois.readObject();
		Class[] parameterTypes = (Class[]) ois.readObject();
		Object[] parameterValues = (Object[]) ois.readObject();
		Method method = obj.getClass().getMethod(methodName, parameterTypes);
		Object returnValue = method.invoke(obj, parameterValues);
		String retType = method.getReturnType().getName();

		ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());

		if (retType.equals("void"))
		{
			oos.writeBoolean(true);
			oos.flush();
			oos.close();
			s.close();
			return;
		}
		else
		{
			oos.writeBoolean(false);
			oos.writeObject(method.getReturnType());
			if (method.getReturnType().isPrimitive())
				writePrimitive(oos, retType, returnValue);
			else
				oos.writeObject(returnValue);

			oos.flush();
			oos.close();
			s.close();
		}
	}

	/**
	 * Write a primitive type via the ObjectOutputStream.
	 * 
	 * @param oos
	 * @param typeName
	 * @param value
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	private void writePrimitive(ObjectOutputStream oos, String typeName, Object value)
			throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		// Get the primitive value of the given primitive type name.
		Object writeVal = getPrimitiveValue(typeName, value);
		// Look up the write method for the given primitive type.
		for (Method method : oos.getClass().getMethods())
			if (method.getName().equalsIgnoreCase("write" + typeName))
			{
				method.invoke(oos, writeVal);
				return;
			}
	}

	/**
	 * Get the primitive value for the given type name via the valueOf method of
	 * the autoboxing class.
	 * 
	 * @param typeName
	 * @param value
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	private Object getPrimitiveValue(String typeName, Object value) throws ClassNotFoundException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		String className = PrimitiveUtils.mapPrimitiveToClass(typeName);
		if (className == null)
			throw new IllegalArgumentException(typeName + " is not a primitive type");
		Class clazz = Class.forName(className);
		for (Method m : clazz.getMethods())
		{
			Class[] parameterTypes = m.getParameterTypes();
			// If the first parameter of the valueOf method is the primitive
			// type, execute it and return.
			if (m.getName().equals("valueOf") && parameterTypes[0].getName().equals(typeName))
				return m.invoke(null, value);
		}

		// We should never get here.
		throw new RuntimeException("Unable to get value of primitive type " + typeName);
	}
}