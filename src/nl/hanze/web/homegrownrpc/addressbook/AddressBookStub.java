package nl.hanze.web.homegrownrpc.addressbook;

public class AddressBookStub extends nl.hanze.web.homegrownrpc.generic.Stub implements
		nl.hanze.web.homegrownrpc.addressbook.AddressBook
{

	public void addStudent(nl.hanze.web.homegrownrpc.addressbook.Student param0) throws Exception
	{
		invokeSkel("addStudent", new Class[]
			{ nl.hanze.web.homegrownrpc.addressbook.Student.class }, new Object[]
			{ param0 });
	}

	public boolean removeStudent(int param0) throws Exception
	{
		return (java.lang.Boolean) invokeSkel("removeStudent", new Class[]
			{ int.class }, new Object[]
			{ param0 });
	}

	@SuppressWarnings("all")
	public java.util.List getAllStudentsAsList() throws Exception
	{
		return (java.util.List) invokeSkel("getAllStudentsAsList", null, null);
	}

	public nl.hanze.web.homegrownrpc.addressbook.Student[] getAllStudentsAsArray() throws Exception
	{
		return (nl.hanze.web.homegrownrpc.addressbook.Student[]) invokeSkel(
				"getAllStudentsAsArray", null, null);
	}

	public int countStudents() throws Exception
	{
		return (java.lang.Integer) invokeSkel("countStudents", null, null);
	}

}
